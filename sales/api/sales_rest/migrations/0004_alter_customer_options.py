# Generated by Django 4.0.3 on 2022-05-13 23:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_alter_salerecord_price'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customer',
            options={'ordering': ('name', 'phone_number', 'address')},
        ),
    ]
